import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Common } from '../Models/common';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { IntentParam } from '../Models/intent-param';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {

  constructor(private httpClient: HttpClient) { }
  getIntentsMeta() {
    return this.httpClient.get<any>(`${Common.ApiURL}/training/IntentsMeta`)
      .pipe(catchError((error: any) => {
        return throwError(error);
      }))
  } 
  getIntents() {
    return this.httpClient.get<any>(`${Common.ApiURL}/training/Intents`)
      .pipe(catchError((error: any) => {
        return throwError(error);
      }))
  }
  getIntentsDetails(intentid: string) {
    return this.httpClient.get<any>(`${Common.ApiURL}/training/Intents?IntentID=${intentid}`)
      .pipe(catchError((error: any) => {
        return throwError(error);
      }))
  }
  SaveIntent(intentParam: IntentParam) {
    intentParam.ProjectID = 'systemtricks-ffxkcf'
    return this.httpClient.post<any>(`${Common.ApiURL}/training/Intents`, intentParam)
      .pipe(catchError((error: any) => {
        return throwError(error);
      }))
  }
  RefreshFromApi() {
    return this.httpClient.get<any>(`${Common.ApiURL}/training/GetIntentsFromAPI`, {})
      .pipe(catchError((error: any) => {
        return throwError(error);
      }))
  }
}
