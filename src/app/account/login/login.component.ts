import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { catchError, map } from 'rxjs/operators';
import { Observable, never } from 'rxjs';
import { vModal } from 'src/app/home/Shared/vikas-modal';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) {
    this.loginForm = this.formBuilder.group({
      UserName: ['', [Validators.required]],
      Password: ['', [Validators.required]]
    });
  }
  loginForm: FormGroup;
  submitted = false;
  invalidBtn = false;
  btnLoginText: string = `Login`;
  ngOnInit() {
    // vModal.Success("hello", function (data) { console.log(data) }, { f: 3, v: 'vikas' });
    // vModal.Warning("hello", function (data) { console.log(data) }, { f: 3, v: 'vikas' });
    // vModal.Error("hello", function (data) { console.log(data) }, { f: 3, v: 'vikas' });
    // vModal.Confirm("hello?", function (data) { console.log('ok'); console.log(data) }, function (data) { console.log('cancle'); console.log(data) }, { f: 3, v: 'vikas' });

    if (!this.authService.IsTokenExpired) {
      this.router.navigateByUrl('/Home');
    }
  }
  get f() { return this.loginForm.controls; }

  LogIN() {
    this.invalidBtn = false;
    if (this.loginForm.invalid) {
      return;
    }
    this.submitted = true;
    this.btnLoginText = `<i class="fa fa-spinner fa-spin" style="font-size:14px"></i> Login`;
    this.authService.LogIN(this.loginForm.value).pipe(catchError((error: any) => {
      this.submitted = false;
      this.btnLoginText = "Login";
      this.invalidBtn = true;
      return new Observable<never>();
    }))
      .subscribe((res) => {
        this.submitted = false;
        this.btnLoginText = "Login";
        console.log("Logged in!");
        this.router.navigateByUrl('Home');
      });
  }
  handleError(error, h: Observable<any>) {
    this.submitted = false;
    this.btnLoginText = "Login";
    return new Observable<never>();
  }

}
