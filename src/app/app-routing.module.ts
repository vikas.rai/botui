import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { UserRole } from './auth/user';
import { AboutComponent } from './about/about.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { BreadCrumbComponent } from './bread-crumb/bread-crumb.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/Account/Login',
    pathMatch: 'full'
  },
  {
    path: 'About',
    component: AboutComponent,
    canActivate: [AuthGuard], data: { roles: [UserRole.SuperAdmin, UserRole.Admin] }
  },
  {
    path: 'Bbb',
    component: BreadCrumbComponent,
    canActivate: [AuthGuard], data: { roles: [UserRole.SuperAdmin, UserRole.Admin] }
  },
  { path: 'Account', loadChildren: './account/account.module#AccountModule' },
  {
    path: 'Home', loadChildren: './home/home.module#HomeModule',
    canActivate: [AuthGuard], data: { roles: [UserRole.Admin] }
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
