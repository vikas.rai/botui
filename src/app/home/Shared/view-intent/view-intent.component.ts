import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { IntentParam, IntentsQuery } from 'src/app/Models/intent-param';
import { TrainingService } from 'src/app/Services/training.service';
import { vModal } from '../vikas-modal';

@Component({
  selector: 'app-view-intent',
  templateUrl: './view-intent.component.html',
  styleUrls: ['./view-intent.component.css']
})
export class ViewIntentComponent implements OnInit, OnChanges {
  ngOnChanges(changes: SimpleChanges): void {/// this method is implemented to check changes logs.
    console.log(changes);
  }

  @Input() data: IntentParam;
  constructor(private _service: TrainingService) { }
  toggled: boolean = false;
  handleSelection(event) {
    console.log(event.char);
    this.toggled = false;
    var input = document.getElementById('ResponsePhrase') as HTMLInputElement
    this.insertAtCursor(input, event.char);
  }
  ngOnInit() {
  }
  addTraining(event: KeyboardEvent) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == 13) {
      let element = event.currentTarget as HTMLInputElement;
      if (element.value.trim() != '') {
        this.data.Training.push({ Request_ID: 0, Request_Description: element.value });
        element.value = "";
      }
    }
  }
  addResponses(event: KeyboardEvent) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == 13 && !event.shiftKey) {
      let element = event.currentTarget as HTMLTextAreaElement;
      if (element.value.trim() != '') {
        this.data.Response.push({ Response_ID: 0, Response_Description: element.value });
        element.value = "";
        return false;
      }
    }
  }
  removeTraining(element: HTMLElement) {

    //var item = (element.previousSibling as HTMLElement).innerHTML; 
    //var item =('.divPhrase').find('span.text').text();
    var value = element.previousSibling.textContent;
    this.data.Training = this.data.Training.filter(function (item) {
      return item.Request_Description != value;
    });
    // var index = this.data.Training.indexOf(item);
    // if (index > -1) {
    //   this.data.Training.splice(index, 1);
    // }
    // //$(thisObj).parents('.divPhrase').remove();
    // element.parentElement
  }
  removeResponse(element: HTMLElement) {
    var value = element.previousSibling.textContent;
    this.data.Response = this.data.Response.filter(function (item) {
      return item.Response_Description != value;
    });
    // var item = element.previousSibling.textContent;
    // var index = this.data.Response.indexOf(item);
    // if (index > -1) {
    //   this.data.Response.splice(index, 1);
    // }
  }
  saveIntent() {
    var btn = document.querySelector('#saveIntentDetails');
    btn.innerHTML = `<i class="fa fa-spinner fa-spin" style="font-size:14px"></i> Save`;
    btn.setAttribute("disabled", "disabled");
    this._service.SaveIntent(this.data).subscribe((res) => {
      btn.innerHTML = 'Save';
      console.log(res);
      btn.removeAttribute("disabled");
      if (res.Status) {
        vModal.Success('Saved successfully.', undefined);
      }
      else {
        vModal.Error(res.IntentApiID, undefined);
      }
    });
  }
  insertAtCursor(input: HTMLInputElement, textToInsert: string) {
    // get current text of the input
    const value = input.value;

    // save selection start and end position
    const start = input.selectionStart;
    const end = input.selectionEnd;

    // update the value with our text inserted
    input.value = value.slice(0, start) + textToInsert + value.slice(end);

    // update cursor to be at the end of insertion
    input.selectionStart = input.selectionEnd = start + textToInsert.length;
  }
}
