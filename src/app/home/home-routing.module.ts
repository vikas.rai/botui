import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LayoutComponent } from './layout/layout.component';
import { TestComponent } from './test/test.component';
import { IntentTrainingComponent } from './intent-training/intent-training.component';

const routes: Routes = [
  { path: '', redirectTo: 'Dashboard', pathMatch: 'full' },
  {
    path: 'Dashboard', component: LayoutComponent,
    children: [
      {path: '', component: DashboardComponent},
      {path: 'Test', component: TestComponent},
      {path: 'Training', component: IntentTrainingComponent},
      { path: '**', redirectTo: '' }]
  },
  { path: '**', redirectTo: 'Dashboard' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
