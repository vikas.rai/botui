import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LayoutComponent } from './layout/layout.component';
import { TestComponent } from './test/test.component';
import { SharedModule } from '../shared/shared.module';
import { IntentTrainingComponent } from './intent-training/intent-training.component';
import { ViewIntentComponent } from './Shared/view-intent/view-intent.component';
import  {  NgxEmojiPickerModule  }  from  'ngx-emoji-picker';

@NgModule({
  declarations: [
    DashboardComponent,
    LayoutComponent,
    TestComponent,
    IntentTrainingComponent,
    ViewIntentComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,    
    NgxEmojiPickerModule.forRoot()
  ]
})
export class HomeModule { }
