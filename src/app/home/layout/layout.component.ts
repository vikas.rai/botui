import { Component, OnInit, HostListener } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Utility } from 'src/app/Models/common';
import { utils } from 'protractor';
import { User } from 'src/app/auth/user';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) {
    setTimeout(() => {
      this.footerResize();
      var el = document.querySelectorAll('.nav-link,main.main,.sidebar-minimizer')
      for (let index = 0; index < el.length; index++) {
        const element = el[index];
        element.addEventListener('click', this.footerResize);
      }
      Utility.tooltip('.navbar-nav [title]', 'left');
    }, 10);

  }
  BreadCrumbList: string[] = [];
  User: User = new User();
  ngOnInit() {
    Utility.CustomScroll('.custom-scroll', 'hidden');
    Utility.loadScript('assets/js/main.js');
    this.BreadCrumb(this.router);
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((data) => {
        this.BreadCrumb(data);
        Utility.CustomScroll('.custom-scroll', 'hidden');
      });
    //var myDropdown = new Utility.Dropdown(document.querySelector('.dropdown'));
    this.User = this.authService.CurrentUser;
    var w = this.User.RoleName;
  }

  LogOut() {
    this.authService.LogOut();
    console.log("Logged Out!");
    this.router.navigateByUrl('Account');

  }
  onDeactivate() {
    document.getElementsByClassName('main')[0].scrollTop = 0;
  }
  BreadCrumb(data: any): void {
    var url = data.url.trim()[0] == '/' ? data.url.trim().slice(1).split('/') : data.url.trim().split('/');
    this.BreadCrumbList.length = 0;
    this.BreadCrumbList.push(...url);
  }

  @HostListener('window:resize')
  footerResize() {
    setTimeout(() => {
      var menuMinimizer = document.querySelector('.sidebar') as any;
      var divFooter = document.querySelector('div.div-footer') as HTMLElement;
      var MenuWidth = menuMinimizer.getClientRects()[0].x < 0 ? 0 : menuMinimizer.clientWidth;
      var left = divFooter.style.left;
      divFooter.style.left = (MenuWidth) + "px";
      var width = divFooter.style.width;
      divFooter.style.width = `calc(100vw - ${MenuWidth}px)`;
      var newwidth = parseInt(getComputedStyle(divFooter).width.replace('px', ''));
      if (newwidth < 250) {
        divFooter.style.width = width;
        divFooter.style.left = left;
      }

      var elScroll = Utility.OverlayScrollbars(document.querySelector('.intent-custom-scroll'));
      if (window.innerWidth <= 576) {
        if (elScroll != undefined) {
          elScroll.destroy();
        }
      }
      else if (elScroll == undefined) {
        Utility.CustomScroll('.intent-custom-scroll', 'hidden');
      }

    }, 300);
  }
}
