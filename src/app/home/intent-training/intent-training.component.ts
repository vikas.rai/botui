import { Component, OnInit } from '@angular/core';
import { IntentParam } from 'src/app/Models/intent-param';
import { TrainingService } from 'src/app/Services/training.service';
import { AuthService } from 'src/app/auth/auth.service';
import { Utility } from 'src/app/Models/common';
import { vModal } from '../Shared/vikas-modal';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-intent-training',
  templateUrl: './intent-training.component.html',
  styleUrls: ['./intent-training.component.css']
})
export class IntentTrainingComponent implements OnInit {
  intents: IntentParam[] = null;
  lstIntents: IntentParam[] = null;
  intentDetails: IntentParam = null;
  showCross = false;
  selectedid = '';
  constructor(private _service: TrainingService, private authenticationService: AuthService) {

  }
  ngOnInit() {
    this.getIntents();
    var txt = document.body.querySelector('#txtAddIntent') as HTMLInputElement
    txt.removeEventListener('keyup', Utility.PreventSpacialChars);
    txt.addEventListener("keyup", Utility.PreventSpacialChars);
    txt.removeEventListener('paste', Utility.PreventSpacialChars);
    txt.addEventListener("paste", Utility.PreventSpacialChars);
  }
  clearSearch() {
    var txtSearch = document.querySelector('#txtSearch') as HTMLInputElement;
    txtSearch.value = '';
    this.searchIntents();
    txtSearch.focus();
  }
  searchIntents() {
    var txtSearch = document.querySelector('#txtSearch') as HTMLInputElement;
    if (txtSearch.value != null && txtSearch.value != '') {
      this.showCross = true;
      this.intents = this.lstIntents.filter(function (intent) {
        return intent.Name.toLowerCase().indexOf(txtSearch.value.toLowerCase()) > -1;
      });
    }
    else {
      this.intents = this.lstIntents;
      this.showCross = false;
    }
  }
  getIntents() {
    this.intents = null;
    this._service.getIntents().pipe(catchError((error: any) => {
      this.intents = [];
      return new Observable<never>();
    })).subscribe((res: IntentParam[]) => {
      console.log(res);
      this.intents = [];
      res.forEach(function (part, index, theArray) {
        if (theArray[index].Training && theArray[index].Training.length > 1) {
          theArray[index].Training = theArray[index].Training.slice(0, 2);
        }
        if (theArray[index].Response && theArray[index].Response.length > 1) {
          theArray[index].Response = theArray[index].Response.slice(0, 2);
        }
      });
      this.lstIntents = res;
      this.intents = this.lstIntents;
      if (this.lstIntents.length == 0) {
        this.intentDetails = new IntentParam();
      }
      else {
        setTimeout(() => {
          var el = document.querySelector('ul.intent-container li') as HTMLElement;
          el.click();
          Utility.tooltip('[title]');
        }, 500);
      }
    });
  }
  intentClick(event) {
    if (event.target.className.indexOf('alert') == -1) {
      var li = document.querySelectorAll('ul.intent-container li');
      li.forEach(el => el.classList.remove('active'));
      var ev = event as Event;
      var el = ev.currentTarget as HTMLElement;
      el.classList.add('active');
      this.intentDetails = null;
      this.selectedid = el.dataset.intentapiid;
      this.getIntentsDetails(el.dataset.id)
    }
  }
  getIntentsDetails(intentid: string) {
    this._service.getIntentsDetails(intentid).pipe(catchError((error: any) => {
      this.intentDetails = new IntentParam();
      return new Observable<never>();
    })).subscribe((res: IntentParam) => {
      //Object.assign(this.intentDetails, res);
      this.intentDetails = res;
      console.log(res);

    });
  }
  addIntentPopup() {
    vModal.Modal('#CreateIntent');
    var txtAddIntent = document.getElementById('txtAddIntent') as HTMLInputElement;
    txtAddIntent.value = "";
    txtAddIntent.classList.remove('is-invalid');
  }
  RefreshFromApi() {
    var btn = document.querySelector('#RefreshFromApi');
    btn.setAttribute("disabled", "disabled");
    (btn.firstChild as HTMLElement).classList.add('fa-spin');
    this._service.RefreshFromApi().pipe(catchError((error: any) => {
      vModal.Error(error, undefined);
      (btn.firstChild as HTMLElement).classList.remove('fa-spin');
      btn.removeAttribute("disabled");
      return new Observable<never>();
    })).subscribe((res) => {
      (btn.firstChild as HTMLElement).classList.remove('fa-spin');
      btn.removeAttribute("disabled");
      if (res.Status) {
        this.getIntents();
        vModal.Success('Completed successfully.', undefined);
      }
      else {
        vModal.Error(res.Message, undefined);
      }
    });
  }
  saveIntent() {
    var txtAddIntent = document.getElementById('txtAddIntent') as HTMLInputElement;
    if (txtAddIntent.value == null || txtAddIntent.value.trim() == '') {
      txtAddIntent.classList.add('is-invalid');
      return;
    }
    var data = new IntentParam();
    data.Name = txtAddIntent.value.trim();
    data.Response = [];
    data.Training = [];
    data.ProjectID = this.authenticationService.CurrentUser.ProjectID;
    data.BotID = this.authenticationService.CurrentUser.BotID;
    var btn = document.querySelector('#addnewintent');
    btn.innerHTML = `<i class="fa fa-spinner fa-spin" style="font-size:14px"></i> Save`;
    btn.setAttribute("disabled", "disabled");
    this._service.SaveIntent(data).pipe(catchError((error: any) => {
      vModal.Error(error, undefined);
      return new Observable<never>();
    })).subscribe((res) => {
      btn.innerHTML = 'Save';
      console.log(res);
      btn.removeAttribute("disabled");
      if (res.Status) {
        txtAddIntent.value = "";
        vModal.CloseModal('#CreateIntent');
        this.lstIntents.push(data);
        this.searchIntents();
        vModal.Success('Saved successfully.', undefined);

        if (this.lstIntents.length == 1) {
          setTimeout(() => {
            var el = document.querySelector('ul.intent-container li') as HTMLElement;
            el.click();
            Utility.tooltip('[title]');
          }, 500);
        }
        else {

        }
      }
      else {
        vModal.Error(res.IntentApiID, undefined);
      }
    });
  }
  cancelIntent() {

  }
  validateIntent(event) {
    setTimeout(() => {
      var txtAddIntent = document.getElementById('txtAddIntent') as HTMLInputElement;
      if (txtAddIntent.value == null || txtAddIntent.value.trim() == '') {
        txtAddIntent.classList.add('is-invalid');
      }
      else {
        txtAddIntent.classList.remove('is-invalid');
      }
    }, 10);

  }
}
