import { Component, OnInit } from '@angular/core';
import { Utility } from 'src/app/Models/common';
import { DashboardService } from 'src/app/Services/dashboard.service';
import { Filter } from 'src/app/Models/filter';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DashboardCounts } from 'src/app/Models/dashboard';
declare var $: any;
declare var Highcharts: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private _service: DashboardService) { }
  counts: DashboardCounts = null;
  tagCloud: any = null;
  filter: Filter = new Filter();

  ngOnInit() {
    eval('window.self1=this');
    // $('.date-picker').datetimepicker({
    //   useCurrent: false,//Important! See issue #1075
    //   format: 'DD-MMM-YYYY',
    //   icons: {
    //     previous: "fa fa-chevron-left",
    //     next: "fa fa-chevron-right",
    //   },
    //   defaultDate: Utility.moment()
    // });
    $('.date-picker').daterangepicker({
      format: 'DD-MMM-YYYY',
      startDate: Utility.moment().subtract(1, 'days'),
      endDate: Utility.moment(),
      showDropdowns: true,
      linkedCalendars: false,
      locale: {
        format: 'DD/MMM/YYYY'
      },
      ranges: {
        'Today': [Utility.moment(), Utility.moment()],
        'Yesterday': [Utility.moment().subtract(1, 'days'), Utility.moment().subtract(1, 'days')],
        'Last 2 Days': [Utility.moment().subtract(1, 'days'), Utility.moment()],
        'Last 7 Days': [Utility.moment().subtract(6, 'days'), Utility.moment()],
        //'Last 30 Days': [Utility.moment().subtract(29, 'days'), Utility.moment()],
        'This Month': [Utility.moment().startOf('month'), Utility.moment().endOf('month')],
        'Last Month': [Utility.moment().subtract(1, 'month').startOf('month'), Utility.moment().subtract(1, 'month').endOf('month')]
      }
    }, function (start, end, label) {
      eval('window.self1.filter.StartDateEpotch=start.startOf("day").unix();')
      eval('window.self1.filter.EndDateEpotch=end.endOf("day").unix();')
      eval('window.self1.BindCounts();window.self1.BindTagCloud();');
      // this.filter.StartDateEpotch=start.startOf('day');
      // this.filter.EndDateEpotch=end.endOf('day');
      console.log("label:" + label + "  A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });
    this.BindCounts();
    this.BindTagCloud();
  }
  BindCounts() {
    this.counts = null;
    this._service.getCounts(this.filter).pipe(catchError((error: any) => {
      this.counts = new DashboardCounts();
      console.log(error);
      return new Observable<never>();
    })).subscribe((res: DashboardCounts) => {
      this.counts = res;
      console.log(res);

    });
  }
  BindTagCloud() {
    this.tagCloud = null;
    $('#tag-cloud').empty();
    this._service.getTagCloud(this.filter).pipe(catchError((error: any) => {
      this.tagCloud = [];
      console.log(error);
      return new Observable<never>();
    })).subscribe((res: any) => {
      this.tagCloud = res;
      console.log(res);
      if (res.length > 0) {
        setTimeout(() => {
          this.drawTagCloud();
        }, 20);

      }
    });
  }
  drawTagCloud() {
    Highcharts.chart('tag-cloud', {
      accessibility: {
        screenReaderSection: {
          beforeChartFormat: '<h5>{chartTitle}</h5>' +
            '<div>{chartSubtitle}</div>' +
            '<div>{chartLongdesc}</div>' +
            '<div>{viewTableButton}</div>'
        }
      },
      series: [{
        type: 'wordcloud',
        data: this.tagCloud,
        name: 'Count',
        rotation: {
          from: 0,
          to: 0,
        }
      }],
      title: {
        text: '',
        align: 'left'
      },
      credits: false
    }
      ,
      function (chart) {//complete
        chart.options.exporting.buttons.contextButton.menuItems.length = 7
        chart.exporting.update();
      }
    );
  }
}
