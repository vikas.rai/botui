export function jwtTokenGetter() {
  return AuthService.JwtToken
}
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtModule} from "@auth0/angular-jwt";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { About1Component } from './about1/about1.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { TokenInterceptor } from './auth/token-interceptor';
import { AuthService } from './auth/auth.service';
import { SharedModule } from './shared/shared.module';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    About1Component,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    JwtModule.forRoot({
      config: {
        //tokenGetter: (): string => { return AuthService.JwtToken },
        tokenGetter: jwtTokenGetter,
        whitelistedDomains: ["ai.systemtricks.com", "localhost:16102"],
        //blacklistedRoutes: ["example.com/examplebadroute/"],
        blacklistedRoutes: [new RegExp('\/oauth\/token.*')]
      }
    })
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  },
  {
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

