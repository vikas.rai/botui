import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-bread-crumb',
  templateUrl: './bread-crumb.component.html',
  styleUrls: ['./bread-crumb.component.css']
})
export class BreadCrumbComponent implements OnInit,OnChanges {
  ngOnChanges(changes: SimpleChanges): void {/// this method is implemented to check changes logs.
    console.log(changes);
  }

 @Input() data: string[];

  ngOnInit() {
  }

}
