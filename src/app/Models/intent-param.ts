export class IntentParam {
    Training: IntentsQuery[];
    Response: IntentMessage[];
    Name: string;
    ProjectID: string;
    IntentApiID:string;
    BotID:number;
    ID:number;
}
export class IntentsQuery {
    Request_ID: number;
    Request_Description: string;
}
export class IntentMessage {
    Response_ID: number;
    Response_Description: string;
}