export enum Common {
    //ApiURL = 'https://ai.systemtricks.com/api'
    ApiURL = 'http://localhost:16102/api'
}
declare var OverlayScrollbars: any;
declare var Tooltip: any;
declare var Dropdown: any;
declare var $: any;
declare var moment: any;

export class Utility {
    static OverlayScrollbars = OverlayScrollbars;
    static moment = moment;
    static tooltip(selector: string, placement = undefined) {
        $(selector).tooltip();
        //var elementsTooltip = document.querySelectorAll(selector);
        // for (var i = 0; i < elementsTooltip.length; i++) {
        //     new Tooltip(elementsTooltip[i], {
        //         placement: placement, //string
        //         animation: 'slideNfade', // CSS class
        //         delay: 150, // integer
        //     })
        // }
    }
    static CustomScroll(selector, x = undefined, y = undefined) {
        var element = document.querySelectorAll(selector);
        var elScroll = OverlayScrollbars(element);
        if (elScroll != undefined)
            for (let index = 0; index < elScroll.length; index++) {
                if (elScroll[index] != undefined) {
                    elScroll[index].destroy();
                }
            }
        OverlayScrollbars(element,
            {
                className: "os-theme-dark",
                overflowBehavior: { x, y },
                scrollbars: { autoHide: "s" }
            }
        );
    }
    static PreventSpacialChars(event: Event) {
        var thisObj = event.currentTarget as HTMLInputElement;
        var AllowedChars = thisObj.dataset.allowedchars;
        var Regx = ' `©~!@#$%^&*()_|+\\-=?;:\'",.<>\\{\\}\\[\\]\\\\\/';
        if (AllowedChars != null && typeof AllowedChars != 'undefined') {
            for (var i = 0; i < AllowedChars.length; i++) {
                Regx = Regx.replace(AllowedChars[i], '');
            }
        }
        Regx = '[' + Regx + ']';
        var Regxp = new RegExp(Regx);
        setTimeout(function () {
            while (Regxp.test(thisObj.value)) {
                var no_spl_char = thisObj.value.replace(Regxp, '');
                thisObj.value = no_spl_char;
            }
        });
    }
    static CheckIsValidUrl(userInput) {
        var regexQuery = "^(https?://)?(((www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6})|((\\d{1,3}\\.){3}\\d{1,3}))(:\\d{2,4})?((/|\\?)(((%[0-9a-f]{2})|[-\\w@\\+\\.~#\\?&/=])*))?$";
        var url = new RegExp(regexQuery, "i");
        return url.test(userInput);
    }
    ValidateNumericValues(thisObj) {
        //var obj = $(thisObj);
        thisObj.val(thisObj.val().replace(/[^0-9\.]/g, ''));
    }
    static loadScript(Url) {
        let body = <HTMLDivElement>document.body;
        let script = document.createElement('script');
        script.innerHTML = '';
        script.src = Url;
        script.async = true;
        script.defer = true;
        body.appendChild(script);
    }
    static ToQueryString(json) {
        return Object.keys(json).map(function (key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
    }
    static validateEmail($email): boolean {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }
}
window.onload = function () {

    eval('window.Utility=Utility');
}

