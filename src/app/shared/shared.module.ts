import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafeHtmlPipe } from '../safe-html.pipe';
import { BreadCrumbComponent } from '../bread-crumb/bread-crumb.component';



@NgModule({
  declarations: [SafeHtmlPipe, BreadCrumbComponent],
  imports: [
    CommonModule,
  ],
  exports:[SafeHtmlPipe,BreadCrumbComponent]
})
export class SharedModule { }
