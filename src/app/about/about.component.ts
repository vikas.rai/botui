import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { UserLogin } from '../auth/user';

@Component({
  selector: 'app-about',
  template: `<i class="fa fa-spinner fa-spin" style="font-size:24px"></i> | safeHtml`,
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private authService: AuthService, ) { }

  btnLoginText: string =`<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>`;
  ngOnInit() {
    this.authService.test(new UserLogin()).subscribe((res) => {
      console.log(res);
      console.log("test");
      //this.router.navigateByUrl('about');
    });
  }
}

