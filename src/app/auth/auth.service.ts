import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { tap, map, catchError } from 'rxjs/operators';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { JwtResponse } from './jwt-response';
import { UserLogin, Client, User, UserRole } from './user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Common } from '../Models/common';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private UserSubject: BehaviorSubject<User>;
  private helper = new JwtHelperService();

  constructor(private httpClient: HttpClient) {
    this.UserSubject = new BehaviorSubject<User>(this.Current);
  }
  public get CurrentUser(): User {
    return this.UserSubject.value;
  }
  private get Current(): User {
    if (AuthService.JwtToken == null) return null;
    var jwtData = this.helper.decodeToken(AuthService.JwtToken);
    try {
      var user: User = AuthService.JwtToken == null ? null : JSON.parse(jwtData.User);
      if (user != null) {
        user.RoleName = jwtData.role;
      }
      return user;
    } catch (error) {
      return null;
    }
  }
  public get IsTokenExpired(): boolean {
    return AuthService.JwtToken == null ? true : this.helper.isTokenExpired(AuthService.JwtToken);
  }
  public get TokenExpiryDate(): Date {
    return this.helper.getTokenExpirationDate(AuthService.JwtToken);
  }
  public static get JwtToken(): string {
    return localStorage.SystemTricksAI ? atob(localStorage.SystemTricksAI) : null;
  }
  LogIN(user: UserLogin) {
    user.grant_type = "password";
    user.client_id = Client.ClientID;
    user.client_secret = Client.ClientSecret;
    localStorage.removeItem('SystemTricksAI');
    localStorage.removeItem('SystemTricksAIww');
    return this.httpClient.post<any>(`${Common.ApiURL}/oauth/token`, this.toQueryString(user), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    }).pipe(tap((JWT: JwtResponse) => {
      // login successful if there's a jwt token in the response
      if (JWT && JWT.access_token) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('SystemTricksAI', btoa(JWT.access_token));
        localStorage.setItem('SystemTricksAIww', btoa(JWT.refresh_token));
        this.UserSubject.next(this.Current);
      }

      return user;
    }), catchError(this.handleError));
  }
  RefreshToken() {
    var user: UserLogin = new UserLogin();
    user.grant_type = "refresh_token";
    user.client_id = Client.ClientID;
    user.client_secret = Client.ClientSecret;
    user.refresh_token = atob(localStorage.SystemTricksAIww);
    return this.httpClient.post<any>(`${Common.ApiURL}/oauth/token`, this.toQueryString(user), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    }).pipe(tap((JWT: JwtResponse) => {
      localStorage.setItem('SystemTricksAI', btoa(JWT.access_token));
      localStorage.setItem('SystemTricksAIww', btoa(JWT.refresh_token));
      this.UserSubject.next(this.Current);
    }), catchError(this.handleError));
  }
  test(user: UserLogin) {
    return this.httpClient.get<any>(`${Common.ApiURL}/api/TestMethod`, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    }).pipe(map(user => {

      return user;
    }), catchError(this.handleError));
  }
  handleError(error: HttpErrorResponse, h: Observable<any>) {
    console.log(error);
    return throwError(error);
  }
  LogOut() {
    localStorage.removeItem('SystemTricksAI');
    localStorage.removeItem('SystemTricksAIww');
    this.UserSubject.next(null);
  }

  toQueryString(obj) {
    var qry = '';
    for (let i = 0; i < Object.keys(obj).length; i++) {
      if (i == 0) {
        qry += Object.keys(obj)[i] + "=" + obj[Object.keys(obj)[i]];
      }
      else {
        qry += "&" + Object.keys(obj)[i] + "=" + obj[Object.keys(obj)[i]];
      }
    }
    return qry;
  }
}
