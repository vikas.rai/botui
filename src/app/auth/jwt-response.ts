export interface JwtResponse {
    access_token: string;
    token_type: string;
    expires_in: number;
    refresh_token: string;
    client_id: string;
    UserName: string;
    Role: string;
    issued: string;
    expires: string;
  }