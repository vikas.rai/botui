import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Observable, BehaviorSubject, throwError, observable } from 'rxjs';
import { catchError, filter, take, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    private isRefreshing = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor(public authService: AuthService, private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {

        // if (this.authService.getJwtToken()) {
        //   request = this.addToken(request, this.authService.getJwtToken());
        // }

        return next.handle(request).pipe(catchError(error => {
            if (error instanceof HttpErrorResponse && error.status === 401) {
                return this.handle401Error(request, next);
            } else {
                return this.HandleError(error);
            }
        }));
    }
    private HandleError(error): Observable<HttpEvent<any>> {
        if (this.router.url.indexOf('Account/Login') == -1) {
            this.router.navigate(['/Account'], { queryParams: { returnUrl: this.router.url } });
            return new Observable<never>();
        }
        else {
            return throwError(error);
        }
    }
    private addToken(request: HttpRequest<any>) {
        return request.clone({
            setHeaders: {
                'Authorization': `Bearer ${AuthService.JwtToken}`
            }
        });
    }
    private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshTokenSubject.next(null);

            return this.authService.RefreshToken().pipe(
                switchMap((token: any) => {
                    this.isRefreshing = false;
                    this.refreshTokenSubject.next(token.access_token);
                    return next.handle(this.addToken(request));
                    //return next.handle(request);
                }),catchError((error: any) => {
                    this.isRefreshing=false;
                    this.router.navigate(['/Account'], { queryParams: { returnUrl: this.router.url } });
                    return new Observable<never>();
                  }));

        } else {
            return this.refreshTokenSubject.pipe(
                filter(token => token != null),
                take(1),
                switchMap(jwt => {
                    return next.handle(this.addToken(request));
                    //return next.handle(request);
                }));
        }
    }
}