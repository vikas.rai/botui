export class User {
    ID: number;
    UserName: string;
    EmailAddress: string;
    MobileNumber: string;
    FirstName: string;
    LastName: string;
    ProjectID: string;
    Role: UserRole;
    RoleName: string;
    Client: ClientDetails;
    BotID:number;
    public get FullName(): string {
        return this.FirstName + " " + this.LastName;
    }
}
export class ClientDetails {
    ClientID: number
    ClientName: string
    ClientFriendlyName: string
    ClientType: number
}
export class UserLogin {
    username: string;
    password: string;
    grant_type: string;
    client_id: string;
    client_secret: string;
    refresh_token: string;
}
export enum UserRole {
    Admin = 1,
    SuperAdmin = 2
}
export enum Client {
    ClientID = 'Angular',
    ClientSecret = '97284E2E-BF9D-4BA3-B3A5-41C8ACB88BBB'
}