var ShowClassNames = ['sidebar-show', 'sidebar-sm-show', 'sidebar-md-show', 'sidebar-lg-show', 'sidebar-xl-show'];
var Event = {
    CLICK: 'click',
    DESTROY: 'destroy',
    INIT: 'init',
    TOGGLE: 'toggle',
    UPDATE: 'update'
};
var ClassName = {
    ACTIVE: 'active',
    BRAND_MINIMIZED: 'brand-minimized',
    NAV_DROPDOWN_TOGGLE: 'nav-dropdown-toggle',
    OPEN: 'open',
    SIDEBAR_FIXED: 'sidebar-fixed',
    SIDEBAR_MINIMIZED: 'sidebar-minimized',
    SIDEBAR_OFF_CANVAS: 'sidebar-off-canvas'
};

var Selector = {
    BODY: 'body',
    BRAND_MINIMIZER: '.brand-minimizer',
    NAV_DROPDOWN_TOGGLE: '.nav-dropdown-toggle',
    NAV_DROPDOWN_ITEMS: '.nav-dropdown-items',
    NAV_ITEM: '.nav-item',
    NAV_LINK: '.nav-link',
    NAV_LINK_QUERIED: '.nav-link-queried',
    NAVIGATION_CONTAINER: '.sidebar-nav',
    NAVIGATION: '.sidebar-nav > .nav',
    SIDEBAR: '.sidebar',
    SIDEBAR_MINIMIZER: '.sidebar-minimizer',
    SIDEBAR_TOGGLER: '.sidebar-toggler'
};
var ps;

function removeClasses(classNames) {
    return classNames.map(function (className) {
        return document.body.classList.contains(className);
    }).indexOf(true) !== -1;
};

function toggleClasses(toggleClass, classNames) {
    var breakpoint = classNames.indexOf(toggleClass);
    var newClassNames = classNames.slice(0, breakpoint + 1);

    var footer = document.querySelector('.div-footer');
    if (removeClasses(newClassNames)) {
        newClassNames.map(function (className) {
            return document.body.classList.remove(className);
        });
        if (window.innerHeight <= 500) {
            footer.classList.add('hide-footer')
        }
        else {
            footer.classList.remove('hide-footer')
        }
    } else {
        document.body.classList.add(toggleClass);
        footer.classList.remove('hide-footer')
    }
};

function _clickOutListener(event) {
    if (!this._element.contains(event.target)) {
        // or use: event.target.closest(Selector.SIDEBAR) === null
        event.preventDefault();
        event.stopPropagation();

        _removeClickOut();

        document.body.classList.remove('sidebar-show');
    }
};

function _addClickOut() {
    document.addEventListener(Event.CLICK, this._clickOutListener, true);
};

function _removeClickOut() {
    document.removeEventListener(Event.CLICK, this._clickOutListener, true);
};

function _toggleClickOut() {
    if (this.mobile && document.body.classList.contains('sidebar-show')) {
        document.body.classList.remove('aside-menu-show');

        _addClickOut();
    } else {
        _removeClickOut();
    }
};

var isInViewport = function (elem) {
    var bounding = elem.getBoundingClientRect();
    return (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};
$(function () {
    $(document).off(Event.CLICK, Selector.SIDEBAR_MINIMIZER);
    $(document).off(Event.CLICK, Selector.SIDEBAR_TOGGLER + ',main.main');
    $(document).on(Event.CLICK, Selector.BRAND_MINIMIZER, function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(Selector.BODY).toggleClass(ClassName.BRAND_MINIMIZED);
    });
    $(document).on(Event.CLICK, Selector.NAV_DROPDOWN_TOGGLE, function (event) {
        event.preventDefault();
        event.stopPropagation();
        var dropdown = event.target;
        $(dropdown).parent().toggleClass(ClassName.OPEN);
    });
    $(document).on(Event.CLICK, Selector.SIDEBAR_TOGGLER + ',main.main', function (event) {
        event.preventDefault();
        event.stopPropagation();
        if (!document.body.classList.contains('sidebar-show') && event.currentTarget.classList.contains('main')) {
            return;
        }
        var toggle = event.currentTarget.dataset ? event.currentTarget.dataset.toggle : $(event.currentTarget).data('toggle');
        if (toggle) {
            toggleClasses(toggle, ShowClassNames);
        }
        else {
            toggleClasses("sidebar-show", ShowClassNames);
        }
        _toggleClickOut();
    })
    $(document).on(Event.CLICK, Selector.SIDEBAR_MINIMIZER, function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(Selector.BODY).toggleClass(ClassName.SIDEBAR_MINIMIZED);

    });
    $(Selector.NAVIGATION + " > " + Selector.NAV_ITEM + " " + Selector.NAV_LINK + ":not(" + Selector.NAV_DROPDOWN_TOGGLE + ")").on(Event.CLICK, function () {
        _removeClickOut();

        document.body.classList.remove('sidebar-show');
    });

});